<?php
function ubah_huruf($string)
{
    // //kode di sini
    $newString = "";
    $len = strlen($string);
    for ($i = 0; $i < $len; $i++) {
        $newString .= chr(ord($string[$i]) + 1);
    }
    return $newString . "\n";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>